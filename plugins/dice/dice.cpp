#include <plugin.hpp>
#include <random>
#include <regex>
#include <algorithm>

#ifndef NDEBUG
#include <iostream>
#endif

class DiceRoller : public BotPlug {

    constexpr static const char *happy = "( ✧Д✧) YES!!";
    constexpr static const char *unhappy = "(╯°□°）╯︵ ┻━┻";

  public:
    std::string execute (const std::string &str)
    {
        int dices = 0;
        int dice_size = 0;
        int modifier = 0;
        int total = 0;
        std::string retval;
        // std::string retval="No dice! Try again.";
        // config.erase (config.begin ());
        try {
            std::regex dice_regex ("roll[ \t]+([0-9]+)d([0-9]+)([-+][0-9]*)?");
            std::smatch cm;

#ifndef NDEBUG
            std::cout << "\"" << str << "\"\n";
#endif
            std::regex_match (str, cm, dice_regex);
/*
 * XdY     3
 * XdY+Z   4
 * XdY-Z   4
 */
#ifndef NDEBUG
            std::cout << "items :" << cm.size () << "\t";
            for (auto shit : cm) {
                std::cout << shit << ",";
            }
            std::cout << "\n";
#endif

            if (cm.size () >= 3) {
                dices = std::stoi (cm[1]);
                dice_size = std::stoi (cm[2]);
            }
            if (cm.size () == 4) {
                try {
                    modifier = std::stoi (cm[3]);
                }
                catch (...) {
                }
            }
            std::vector<int> values;

            std::random_device rd;
            std::uniform_int_distribution<int> dist (1, dice_size);
            int max_counter = 0;
            int min_counter = 0;
            for (int i (0); i < dices; i++) {
                int val = dist (rd);
                if (val == dice_size) {
                    max_counter++;
                }
                else if (val == 1) {
                    min_counter++;
                }
                values.push_back (val);
            }
            
            std::sort(values.begin(), values.end());

            std::ostringstream out;
            for (auto val : values) {
                out << "[" << val << "] ";
                total += val;
            }
            if (modifier != 0) {
                if (modifier > 0) {
                    out << "+";
                }
                out << modifier;
                total += modifier;
            }
            if (!values.empty ()) {
                out << " = " << total;

                if (dice_size >= 4) {
                    if (max_counter == values.size ()) {
                        out << " " << happy;
                    }
                    else if (min_counter == values.size ()) {
                        out << " " << unhappy;
                    }
                }
                retval = out.str ();
            }
        }
        catch (const std::exception &e) {
            std::cerr << e.what () << "\n";
        }
        if (retval.empty ()) {
            return unhappy;
        }
        return retval;
    }
};


static class DiceRollerFactory : public BotPlugFactory {
  public:
    BotPlug_ptr create (boost::asio::io_service& io_service)
    {
        BotPlug_ptr retval = std::make_shared<DiceRoller> ();
        return retval;
    }
} factory;

extern "C" void registerPlugin (BotPlugManager &PM) { PM.registerClass ("roll", &factory, "Dice rolling functionality."); }
