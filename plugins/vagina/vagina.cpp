#include <plugin.hpp>
#include <random>
#include <regex>

#ifndef NDEBUG
#include <iostream>
#endif

class Vagina1 : public BotPlug {

    constexpr static const char *vagina1 = 
"<pre><br/>"
"   ~~~~~~~~~~~~~<br/>"
"~~~~~~~ / \\~~~~~~~~<br/>"
"~~~~~~~/ 0 \\~~~~~~~<br/>"
"~~~~~~( ) ( )~~~~~~<br/>"
"~~~~~(  )  ( )~~~~~<br/>"
"~~~~(  )    ( )~~~~<br/>"
"~~~(  ) 000  ( )~~~<br/>"
"~~~~( ) 000  ( )~~~<br/>"
"~~~~~( )000 ( )~~~~<br/>"
"~~~~~~( )  ( )~~~~~<br/>"
"~~~~~~~() ()~~~~~~~<br/>"
"~~~~~~~~(_)~~~~~~~~<br/>"
"~~~~~~~~~~~~~~~~~~~<br/>"
"        **<br/>"
"</pre>";

  public:
    std::string execute (const std::string &str)
    {
        return vagina1;
    }
};


static class VaginaFactory : public BotPlugFactory {
  public:
    BotPlug_ptr create (boost::asio::io_service& io_service)
    {
        BotPlug_ptr retval = std::make_shared<Vagina1> ();
        return retval;
    }
} factory;

extern "C" void registerPlugin (BotPlugManager &PM) { PM.registerClass ("vagina", &factory, "An ASCII vagina."); }
