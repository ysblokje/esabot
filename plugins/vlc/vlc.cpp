#include <boost/tokenizer.hpp>
#include <functional>
#include <plugin.hpp>
#include <vlc/vlc.h>


typedef std::function<void(const void *samples, unsigned count, int64_t pts)> vlc_audio_callback_t;


class VLCBase : public BotPlug {
  public:
    VLCBase (vlc_audio_callback_t vlc_audio_callback);
    VLCBase ();

    std::string execute (const std::string &txt)
    {
        std::string retval;
        std::vector<std::string> v;
        boost::tokenizer<boost::escaped_list_separator<char>> tok (txt, boost::escaped_list_separator<char> ('\\', ' ', '\"'));
        std::for_each (tok.begin (), tok.end (), [&v](const std::string &s) { v.push_back (s); });

        if (v[1] == "current") {
            retval = uri;
        }
        else if (v[1] == "play") {
            if (v.size () > 2)
                startURI (v[2]);
            else {
                retval = "Please provide a location.";
            }
        }
        else if (v[1] == "pause") {
            pause ();
        }
        else if (v[1] == "unpause") {
            unpause ();
        }
        else if (v[1] == "stop") {
            if (player) {
                libvlc_media_player_stop (player);
            }
        }
        return retval;
    }
    void setAudioCallback (vlc_audio_callback_t);
    void startURI (const std::string &playlist);
    void pause ();
    void unpause ();
    ~VLCBase ();
    void vlcAudioIn (const void *samples, unsigned count, int64_t pts);
    libvlc_media_player_t *getPlayer ();
    inline const vlc_audio_callback_t &getACB () { return m_vlc_audio_callback; }

  protected:
    vlc_audio_callback_t m_vlc_audio_callback;
    std::string uri;
    libvlc_instance_t *instance = nullptr;
    libvlc_media_player_t *player = nullptr;
};

static void play_cb (void *data, const void *samples, unsigned count, int64_t pts)
{
    VLCBase *ptr = static_cast<VLCBase *> (data);
    ptr->vlcAudioIn (samples, count, pts);
}


VLCBase::VLCBase (vlc_audio_callback_t vlc_audio_callback) : m_vlc_audio_callback (vlc_audio_callback) {}

void VLCBase::setAudioCallback (vlc_audio_callback_t vlc_audio_callback) { m_vlc_audio_callback = vlc_audio_callback; }

VLCBase::VLCBase () {}

void VLCBase::startURI (const std::string &playlist)
{
    if (playlist.empty ()) return;
    uri = playlist;
    libvlc_media_t *media = nullptr;
    instance = libvlc_new (0, nullptr);
    media = libvlc_media_new_location (instance, uri.c_str ());
    player = libvlc_media_player_new_from_media (media);
    libvlc_audio_set_callbacks (player, play_cb, nullptr, nullptr, nullptr, nullptr, this);
    libvlc_audio_set_format (player, "S16N", 48000, 1);
    libvlc_audio_set_volume (player, 100);
    libvlc_media_release (media);
    if (player) libvlc_media_player_play (player);
}
void VLCBase::pause ()
{
    if (player) libvlc_media_player_set_pause (player, 1);
}
void VLCBase::unpause ()
{
    if (player) libvlc_media_player_set_pause (player, 0);
}

VLCBase::~VLCBase ()
{
    if (player) {
        libvlc_media_player_stop (player);
        libvlc_media_player_release (player);
        libvlc_release (instance);
    }
}
#include <fstream>
void VLCBase::vlcAudioIn (const void *samples, unsigned count, int64_t pts)
{
    if (sendAudio) {
        AudioPacket<> *afd;
        afd = new AudioPacket<> (count, 1, 48000, samples);
        sendAudio (afd);
    }
}

libvlc_media_player_t *VLCBase::getPlayer () { return player; }


static class VLC : public BotPlugFactory {
  public:
    BotPlug_ptr create (boost::asio::io_service &io_service)
    {
        BotPlug_ptr retval = std::make_shared<VLCBase> ();
        return retval;
    }
} factory;

extern "C" void registerPlugin (BotPlugManager &PM) { PM.registerClass ("vlc", &factory, "audio streaming using libvlc", true); }
