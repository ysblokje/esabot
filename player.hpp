#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__
#include "mumlib.hpp"
#include <audiopackets.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <log4cpp/Category.hh>
#include <functional>
#include <string>

struct Player {
    log4cpp::Category &logger = log4cpp::Category::getInstance (std::string ("AudioTransmitter"));
    boost::asio::deadline_timer timer;

    mumlib::Mumlib *mum = nullptr;
    AudioFifo<float> *m_audiofifo = nullptr;

    bool running = false;
    size_t output_frame_size;
    std::function<void(const boost::system::error_code &e)> timeout_handler;
    Player (boost::asio::io_service &io_service, size_t output_frame_size)
    : timer (io_service), output_frame_size (output_frame_size)

    {
        timeout_handler = std::bind (&Player::timeout, this, std::placeholders::_1);
    }

    //void addAudio (AudioPacket<> *packet) { m_audiofifo->append (packet); }

    void start ()
    {
        if (running) return;

        timer.expires_from_now (boost::posix_time::seconds (2));
        timer.async_wait (timeout_handler);
    }

    void timeout (const boost::system::error_code &e)
    {
        if (m_audiofifo) {
            auto framecount = m_audiofifo->getNoOfFrames ();
            if (framecount >= output_frame_size) {
                if (mum != nullptr && mum->getConnectionState () == mumlib::ConnectionState::CONNECTED) {
                    timer.expires_from_now (boost::posix_time::milliseconds (60));
                    auto packet = m_audiofifo->getAudio ();


                    mum->sendAudioData (packet->m_samples, output_frame_size, mumlib::FLOAT);
                    delete packet;
                }
            }
            else {
                timer.expires_from_now (boost::posix_time::seconds (2));
            }
        } else {
            timer.expires_from_now (boost::posix_time::seconds (2));
        }
        timer.async_wait (timeout_handler);
    };
};
#endif //__PLAYER_HPP__
