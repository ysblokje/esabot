#ifndef __MENU_HPP__
#define __MENU_HPP__
#include <string>
#include <vector>

typedef std::function<void ()> MenuFunction_t;

struct MenuItem {
    std::string m_name;
    bool hasSub() {
        return !m_subitems.empty();
    }
    MenuFunction_t m_executer;
    typedef std::vector<MenuItem> MenuItems;
    MenuItems m_subitems;

    MenuItem(const std::string &name, const MenuFunction_t &executer) :
        m_name(name), m_executer(executer) {}

    MenuItem(const std::string &name, const std::vector<MenuItem> &subitems) :
        m_name(name), m_subitems(subitems) {}
    MenuItem(const std::string &name) : m_name(name) {}
};

typedef MenuItem::MenuItems MenuItems;


#endif //__MENU_HPP__
