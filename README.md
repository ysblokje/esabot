This is the mumble-jukebox project, the result of some fooling around with mumlib and libspotify.

Things you should know are :

* ugly hack
* probably has numerous bugs
* leaks somewhat
* because of the previous point code should be cleaned up and other sources of music should be added
* mono audio (haven't figured out if mumlib even supports stereo audio)
* can be controlled from every mumbleclient
* does not daemonize yet
* very verbose about what it's doing
* contains code blatenly copied from the example programs
* has volume control
* has a help function
* understands private messages
* replies in private to those messages
* NEEDS a fork of mumlib
  * original : https://github.com/slomkowski/mumlib
  * my fork : https://github.com/ysblokje/mumlib
* IT WORKS, we've been running it for days/weeks on end on private mumbleservers

* unlike it's predecessor it supports a plugin structure now.

With that out of the way, expect more features to come when I and the people that use it decide we want it ;)

Getting it compiled is quite easy when you have mumlib and libspotify installed.

    git clone https://github.com/ysblokje/esabot.git
    cd esabot
    git submodule init
    git submodule update

    mkdir build
    cd build
    cmake <srclocation>
    make
    ./esabot <mumbleserverip> <password> <username>

Ysblokje
