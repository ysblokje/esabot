#ifndef __PLUGIN_HPP__
#define __PLUGIN_HPP__
#include <memory>
#include <audiopackets.hpp>
#include "plugin_factory.hpp"
#include "plugin_manager.hpp"


class BotPlug {
  public:
    virtual std::string execute (const std::string &) = 0;
    std::function<void (AudioPacket<>*)>  sendAudio;
//    virtual AudioPacket<> *getAudio () {
        //return nullptr;
    //}
};

using BotPlug_ptr = std::shared_ptr<BotPlug>;
using BotPlugManager = PluginManager<BotPlug_ptr>;
using BotPlugFactory = BotPlugManager::pf_type;

#endif // __PLUGIN_HPP__
