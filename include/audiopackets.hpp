#ifndef __AUDIOPACKETS_HPP__
#define __AUDIOPACKETS_HPP__
#include <algorithm>
#include <condition_variable>
#include <cstring>
#include <list>
#include <mutex>
//
// Heavily borrowed from the spotify jukebox example
//

template <typename S_TYPE = int16_t> struct AudioPacket {
    int m_channels = 0;
    int m_rate = 0;
    int m_nsamples = 0;
    S_TYPE *m_samples = nullptr;

    AudioPacket (int num_frames, unsigned short channels, int samplerate, const void *frames = nullptr)
    {
        size_t s = num_frames * sizeof (S_TYPE) * channels;

        m_samples = new S_TYPE[s];
        if (frames) memcpy (m_samples, frames, s);

        m_nsamples = num_frames;
        m_rate = samplerate;
        m_channels = channels;
    }
    ~AudioPacket ()
    {
        if (m_samples) delete[] m_samples;
    }
};

template <typename S_TYPE = int16_t> struct AudioFifo {
    void append (AudioPacket<S_TYPE> *p)
    {
        if(!m_discard) {
        {
            std::lock_guard<std::mutex> lock (mutex);
            queue.push_back (p);
            m_frames += p->m_nsamples;
        }
        cond.notify_one ();
        }
    }
    void flush ()
    {
        std::lock_guard<std::mutex> lock (mutex);
        std::for_each (queue.begin (), queue.end (), [](AudioPacket<S_TYPE> *ap) { free (ap); });
        queue.clear ();
        m_frames = 0;
    }
    
    void discard(bool setting) {
        m_discard = setting;
    }

    AudioPacket<S_TYPE> *getAudio ()
    {
        AudioPacket<S_TYPE> *afd (nullptr);
        std::unique_lock<std::mutex> lock (mutex);

        while (m_frames == 0) {
            cond.wait (lock, [this] { return m_frames != 0; });
        }

        afd = queue.front ();
        queue.pop_front ();
        m_frames -= afd->m_nsamples;

        lock.unlock ();
        return afd;
    }

    int getNoOfFrames ()
    {
        std::lock_guard<std::mutex> lock (mutex);
        return m_frames;
    }

  private:
    std::list<AudioPacket<S_TYPE> *> queue;
    int m_frames = 0;
    std::mutex mutex;
    std::condition_variable cond;
    bool m_discard = false;
};

#endif //__AUDIOPACKETS_HPP__
