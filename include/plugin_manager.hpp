#ifndef __PLUGIN_MANAGER_HPP__
#define __PLUGIN_MANAGER_HPP__
#include <boost/filesystem.hpp>
#include <iostream>
#include <map>
#include <memory>
#include <string>

#include "plugin.hpp"
#include "plugin_factory.hpp"

template <typename INTERFACE> class PluginManager {
  public:
    using Interface_ptr = std::shared_ptr<INTERFACE>;
    using pf_type = PluginFactory<INTERFACE>;


    struct PFInfo {
        PFInfo (pf_type *factory, std::string comment, bool audio_provider=false) : factory (factory), comment (comment), audio_provider(audio_provider) {}
        pf_type *factory;
        std::string comment;
        bool audio_provider;
    };

    using PFPair = std::pair<std::string, PFInfo>;
    using PFMap = std::map<std::string, PFInfo>;

    void registerClass (const std::string &name, PluginFactory<INTERFACE> *PF, const std::string &comment, bool audio_provider=false)
    {
        if (pfmap.find (name) == pfmap.end ()) {
            PFPair pair (name, PFInfo{ PF, comment, audio_provider });
            pfmap.insert (pair);
        }
    }
    
    void flushFactories() {
        pfmap.clear();
    }

    INTERFACE getInstance (const std::string &name, boost::asio::io_service& io_service)
    {
        INTERFACE retval;
        auto it (pfmap.find (name));
        if (it != pfmap.end ()) {
            retval = (*it).second.factory->create (io_service);
        }
        return retval;
    }
    const PFMap getFactories () { return pfmap; }

  private:
    PFMap pfmap;
};


#include <dlfcn.h>
template <typename INTERFACE> void scanMap (const std::string &map, PluginManager<INTERFACE> &PM)
{
    typedef void (*func_t) (PluginManager<INTERFACE> &);

    try {
        boost::filesystem::path p (map);
        boost::filesystem::recursive_directory_iterator di (p), end;

        while (di != end) {
            if (boost::filesystem::is_regular_file (di->path ())) {
                void *handle = dlopen ((*di).path ().string ().c_str (), RTLD_NOW);
                if (handle) {
#ifndef NDEBUG
                    std::cerr << di->path () << "\n";
#endif
                    func_t fun = (func_t)dlsym (handle, "registerPlugin");
                    if (fun) {
#ifndef NDEBUG
                        std::cerr << (*di).path ().string () << "\n";
#endif
                        (*fun) (PM);
                    }
                }
            }
            di++;
        }
    }
    catch (...) {
    }
}

#endif // __PLUGIN_MANAGER_HPP__
