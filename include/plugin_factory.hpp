#ifndef __PLUGIN_FACTORY_HPP__
#define __PLUGIN_FACTORY_HPP__
#include <memory>
#include <boost/asio/io_service.hpp>

template <typename INTERFACE> class PluginFactory {
  public:
    virtual INTERFACE create (boost::asio::io_service&) = 0;
};

#endif // __PLUGIN_FACTORY_HPP__
