#ifndef __AUDIOCONVERTER_HPP
#define __AUDIOCONVERTER_HPP
#include <thread>
#include "log4cpp/Category.hh"
#include "mumlib.hpp"
#include <audiopackets.hpp>
#include <soxr.h>

template<typename AUDIOPROVIDER>
struct AudioConverter {
    log4cpp::Category &logger = log4cpp::Category::getInstance (std::string ("AudioConverter"));

    typedef AudioFifo<float> AudioFifo_type;
    AUDIOPROVIDER* provider=nullptr;
    mumlib::Mumlib &mum;
    std::thread *t = nullptr;
    size_t max_frames_in_audiopacket = 2880;
    AudioFifo_type output_buffer;
    float volume = 0.25f;
    int max_frames_in_buffer = 48000 * 2;
    int output_rate = 48000;

    AudioConverter (size_t max_frames_in_audiopacket, size_t maximum_output_buffer_duration, mumlib::Mumlib &mum)
    : max_frames_in_audiopacket (max_frames_in_audiopacket), mum (mum)
    {
        max_frames_in_buffer = output_rate * maximum_output_buffer_duration;
    }

    void changeRate (int rate) { output_rate = rate; }

    void changeFrameCount (int count) { max_frames_in_audiopacket = count; }

    void changeVolume (float v) { volume = v; }
    AudioFifo<float> *getAudioFifo () { return &output_buffer; }

    void start () { t = new std::thread (&AudioConverter::play, this); }

    void play ()
    {
        int16_t *output;

        int err;
        soxr_t soxr;
        soxr_error_t sox_error;
        int current_rate = 0;
        size_t total_odone (0);

        AudioPacket<> *ap_in (nullptr);
        AudioPacket<float> *ap_out (nullptr);
        std::vector<float> input_buffer;

        float m = (1.0f / 32768.f);
        ap_out = new AudioPacket<float> (max_frames_in_audiopacket, 1, output_rate, nullptr);
        for (;;) {
            try {
                if (output_buffer.getNoOfFrames () > (max_frames_in_buffer)) std::this_thread::sleep_for (std::chrono::seconds (1));
                if(provider) {
                    ap_in = provider->getAudio ();                    
                }

                if (ap_in) {
                    if (ap_in->m_rate != current_rate) {
                        logger.notice ("rate changed from %i to %i", current_rate, ap_in->m_rate);
                        soxr = soxr_create (ap_in->m_rate, output_rate, 1, &sox_error, NULL, NULL, NULL);
                        current_rate = ap_in->m_rate;
                    }
                    if (mum.getConnectionState () == mumlib::ConnectionState::CONNECTED) {
                        input_buffer.clear ();


                        if (ap_in->m_channels == 1) {
                            for (size_t i (0); i < ap_in->m_nsamples; i++) {
                                int32_t l = ap_in->m_samples[i];
                                input_buffer.push_back (m * (l * volume));
                            }
                        }
                        else if (ap_in->m_channels == 2) {
                            for (size_t i (0); i < ap_in->m_nsamples; i++) {
                                int32_t l = ap_in->m_samples[i << 1];
                                int32_t r = ap_in->m_samples[(i << 1) + 1];
                                input_buffer.push_back (m * (((l + r) >> 1) * volume));
                            }
                        }
                    }
                    delete ap_in;


                    if (mum.getConnectionState () == mumlib::ConnectionState::CONNECTED) {
                        size_t odone (0), idone (0), idone_total (0);

                        while (input_buffer.size () != idone_total) {
                            sox_error = soxr_process (soxr, &(input_buffer[idone_total]), input_buffer.size () - idone_total, &idone,
                                                      ap_out->m_samples + total_odone, max_frames_in_audiopacket - total_odone, &odone);
                            idone_total += idone;
                            total_odone += odone;

                            if (total_odone == max_frames_in_audiopacket) {
                                output_buffer.append (ap_out);
                                ap_out = new AudioPacket<float> (max_frames_in_audiopacket, 1, output_rate, nullptr);
                                total_odone = 0;
                            }
                        }
                    }
                    else {
                    }
                }
            }
            catch (const std::exception &e) {
                std::cerr << "BOEM " << e.what () << std::endl;
            }
        }
    }
};
#endif //__AUDIOCONVERTER_HPP
