cmake_minimum_required(VERSION 3.3.0)
set(CMAKE_CODELITE_USE_TARGETS ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)
set(CMAKE_CXX_STANDARD 20) 

project(esabot)


INCLUDE(FindPkgConfig)
find_package(PkgConfig REQUIRED)
find_package(Boost COMPONENTS filesystem system REQUIRED)
pkg_check_modules(LOG4CPP "log4cpp")

add_subdirectory(mumlib)
include_directories(${Boost_INCLUDE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${LOG4CPP_INCLUDE_DIRS})
include_directories(include)
add_subdirectory(plugins)

add_executable(esabot esabot.cpp player.hpp include/audioconverter.hpp include/audiopackets.hpp)
target_link_libraries(esabot mumlib soxr ssl pthread crypto log4cpp ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} dl) 
target_include_directories(esabot PRIVATE $<TARGET_PROPERTY:mumlib,INTERFACE_INCLUDE_DIRECTORIES>)

