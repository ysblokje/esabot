#include "audioconverter.hpp"
#include "mumlib.hpp"
#include "player.hpp"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/tokenizer.hpp>
#include <chrono>
#include <condition_variable>
#include <log4cpp/Category.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/OstreamAppender.hh>
#include <mumlib/Transport.hpp>
#include <mutex>
#include <signal.h>
#include <thread>
#include <chrono>
#include <plugin.hpp>

#define OUTPUTFRAMESIZE 2880

static std::mutex transmit_mutex;
static std::string username = "EsaBot";

class MyCallback : public mumlib::BasicCallback {
    log4cpp::Category &logger = log4cpp::Category::getInstance (std::string ("Callback"));

    AudioConverter<AudioFifo<>> *converter;
    AudioFifo<> audio_in;
    BotPlugManager botplugmanager;
    std::map<std::string, BotPlug_ptr> loaded_plugins;
    boost::asio::io_service &ioservice;
    mumlib::Mumlib *mum;
    int32_t channel_id = 0;
    int32_t session_id = -1;
    Player player;

  public:
    MyCallback (boost::asio::io_service &ioservice) : ioservice (ioservice), player (ioservice, OUTPUTFRAMESIZE)
    {
        doScan();
    }
    
    void doScan() { 
        loaded_plugins.clear();
        botplugmanager.flushFactories();
        scanMap ("./plugins", botplugmanager);
        //for(auto plugin : )
    }

    void setMumLib (mumlib::Mumlib *mum)
    {
        this->mum = mum;
        converter = new AudioConverter<AudioFifo<>> (OUTPUTFRAMESIZE, 2, *mum);
        converter->provider = &audio_in;
        player.mum = mum;
        player.m_audiofifo = converter->getAudioFifo ();
        player.start ();
        converter->start ();
    }

    virtual void audio (int target, int sessionId, int sequenceNumber, int16_t *pcm_data, uint32_t pcm_data_size) override {}
    virtual void permissionDenied (int32_t permission, int32_t channel_id, int32_t session, std::string reason, int32_t deny_type, std::string name)
    {
        logger.warn ("Permission DENIED ! : %s %s", reason.c_str (), name.c_str ());
    };

    virtual void serverSync (std::string welcome_text, int32_t session, int32_t max_bandwidth, int64_t permissions)
    {
        logger.notice ("%s , id=%i", welcome_text.c_str (), session);
        session_id = session;
    };

    template <typename T> std::string printV (const T &v)
    {
        auto it (v.begin ());
        std::ostringstream out;
        while (it != v.end ()) {
            out << " " << *it;
            it++;
        }

        return out.str ();
    }

    virtual void
    textMessage (uint32_t actor, std::vector<uint32_t> session, std::vector<uint32_t> channel_id, std::vector<uint32_t> tree_id, std::string message) override
    {
        try {
            bool private_msg = false;
            logger.notice ("received : %s", message.c_str ());
            logger.notice ("actor : %i", actor);
            logger.notice ("session %s", printV (session).c_str ());
            logger.notice ("channel_id %s", printV (channel_id).c_str ());
            logger.notice ("tree_id %s", printV (tree_id).c_str ());

            if (!session.empty () && session[0] == session_id) {
                private_msg = true;
            }

            boost::replace_all (message, "&quot;", "\"");
            std::string stripped_msg;
            bool copy (true);
            for (auto character : message) {
                switch (character) {
                    case '<':
                        copy = false;
                        break;
                    case '>':
                        copy = true;
                        break;
                    default:
                        if (copy) {
                            stripped_msg += character;
                        }
                        break;
                }
            }
            if (stripped_msg.size () >= 1 && stripped_msg[0] == '/') {
                stripped_msg.erase (stripped_msg.begin ());
                std::vector<std::string> v;
                boost::tokenizer<boost::escaped_list_separator<char>> tok (stripped_msg, boost::escaped_list_separator<char> ('\\', ' ', '\"'));
                std::for_each (tok.begin (), tok.end (), [&v](const std::string &s) { v.push_back (s); });
                std::string reply;
                {
                    BotPlug_ptr plugin_ptr;
                    bool plugin = false;
                    auto loaded_it = loaded_plugins.find (v[0]);
                    if (loaded_it == loaded_plugins.end ()) {
                        plugin_ptr = botplugmanager.getInstance (v[0], ioservice);
                        if (plugin_ptr) {
                            plugin_ptr->sendAudio = [this](AudioPacket<> *packet) { audio_in.append (packet); };
                            loaded_plugins.insert (std::make_pair (v[0], plugin_ptr));
                        }
                    }
                    else {
                        plugin_ptr = loaded_it->second;
                    }
                    if (plugin_ptr) {
                        plugin = true;
                        reply = plugin_ptr->execute (stripped_msg);
                    }

                    if (!plugin) {
                        if (v.size () >= 1) {
                            if (v[0] == "help") {
                                reply = "<em>available commands</em><br/>"
                                        "move<br/>"
                                        "plugins<br/>"
                                        "shutup<br/>"
                                        "hey<br/>"
                                        "volume<br/>";
                            }
                            else if (v[0] == "volume") {
                                try {
                                    switch (v.size ()) {
                                        default:
                                            reply = "Please use /volume possibly followed by a number from 1 to 100.";
                                            break;
                                        case 1:
                                            if (converter != nullptr) {
                                                reply = "Current volume setting : " + std::to_string (converter->volume * 100);
                                            }
                                            else {
                                                reply = "No audio playing";
                                            }
                                            break;
                                        case 2: {
                                            float percentage = std::stof (v[1]);
                                            float volume = (percentage / 100);
                                            if (volume > 1.0) volume = 1.0;
                                            if (volume < 0) volume = 0.0;
                                            if (converter) converter->changeVolume (volume);
                                        } break;
                                    }
                                }
                                catch (...) {
                                    reply = "Something was wrong with you query.";
                                }
                            }
                            else if (v[0] == "plugins") {
                                std::ostringstream stream;
                                stream << "<table>";
                                stream << "<th><td>name</td><td>description</td></th>";
                                for (auto it : botplugmanager.getFactories ()) {
                                    stream << "<tr>";
                                    stream << "<td>" << it.first << "</td>";
                                    stream << "<td>" << it.second.comment << "</td>";
                                    stream << "<td>";
                                }
                                stream << "</table>";
                                reply = stream.str ();
                            }
                            else if (v[0] == "move") {
                               // mum->joinChannel (1);
                            }
                            else if (v[0] == "shutup") {
                                audio_in.discard(true);
                            }
                            else if (v[0] == "hey") {
                                audio_in.discard(false);
                            }
                        }
                    }
                }
                if (!reply.empty ()) {
                    {
                        logger.notice ("reply %s", reply.c_str ());
                        if (private_msg) {
                            mum->sendTextMessage (std::vector<uint32_t> () = { actor }, channel_id, tree_id, reply);
                        }
                        else {
                            mum->sendTextMessage (std::vector<uint32_t> () = { static_cast<uint32_t>(session_id) }, channel_id, tree_id, reply);
                        }
                    }
                }
            }
        }
        catch (...) {
        }
    }
};

static std::mutex output_mutex;
static std::condition_variable output_con;
static std::vector<int16_t> output_buffer;
static std::string current_track;


bool global_stop (false);

extern "C" void stop_signal (int s) { global_stop = true; }
int main (int argc, char *argv[])
{
    log4cpp::Appender *appender1 = new log4cpp::OstreamAppender ("console", &std::cout);
    appender1->setLayout (new log4cpp::BasicLayout ());
    log4cpp::Category &logger = log4cpp::Category::getRoot ();
    logger.setPriority (log4cpp::Priority::NOTICE);
    logger.addAppender (appender1);
    if (argc < 3) {
        logger.crit ("Usage: %s <server> <password> <mumbleusername> [service] [accountname] [accountpassword]", argv[0]);
        return 1;
    }
    username = argv[3];

    signal (SIGINT, stop_signal);


    boost::asio::io_service io_service;
    MyCallback myCallback (io_service);
    mumlib::MumlibConfiguration conf;
    conf.opusEncoderBitrate = 64000;
    mumlib::Mumlib mum (myCallback, io_service, conf);
    myCallback.setMumLib (&mum);
    boost::asio::deadline_timer timer (io_service);
    std::function<void(const boost::system::error_code &e)> timeout_handler;
    timeout_handler = [&timer, &io_service, &timeout_handler](const boost::system::error_code &e) {

        if (global_stop)
            io_service.stop();
        else {
            timer.expires_from_now (boost::posix_time::seconds (1));
            timer.async_wait (timeout_handler);
        }

    };

    bool dontstop = true;
    while (dontstop) {
        try {
            mum.connect (argv[1], 64738, username.c_str (), argv[2]);
            timeout_handler (boost::system::error_code ());
            io_service.run ();
        }
        catch (mumlib::TransportException &exp) {
            logger.error ("TransportException: %s.", exp.what ());
            mum.disconnect();
            std::this_thread::sleep_for( std::chrono::seconds(1));
        }
    }
}
